# PageFound

## Why ?

`PageFound` is a static analyzer that parses files (html, markdown, TeX, ...)
to find urls, test them and avoid the famous `Page Not Found` errors.

This is quite useful when you want to test your documentation, a scientific
paper, a website with external links, and so on.

## When ?

It can be triggered manually, with pre-commit, or on a CI process.

## How ?

### Installation

`PageFound` requires python 3.8 or above.

```bash
pip install git+https://gitlab.inria.fr/vrouvrea/pagefound
```

### Run

To run `PageFound` on all files of the current directory recursively :
```bash
python -m pagefound
```

`PageFound` can also be launched on a specific file and/or a directory
recursively and/or glob patterns (`*.md`, `**/*.html`, ...):
```bash
python -m pagefound {source_file and/or source_directory and/or source_glob_pattern}
```

Please consult `pagefound --help` for more options.
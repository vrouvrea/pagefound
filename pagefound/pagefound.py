import click
import sys
import re  # for regexp
from urllib.request import urlopen, Request
from urllib.error import URLError, HTTPError
from pathlib import Path

# https://github.com/django/django/blob/stable/1.3.x/django/core/validators.py#L45
_regex = re.compile(
    r'^(?:http|ftp)s?://' # http:// or https://
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
    r'localhost|' #localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
    r'(?::\d+)?' # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE)

def _url_open(url: str, verbose: bool, timeout: int, line_number: int) -> int:
    # Maybe the _regex could be improved
    if url[-1] == '.':
        url = url[:-1]
    if re.match(_regex, url) is None:
        click.secho(f"{url} is not valid", fg='red')
    else:
        try:
            opened_url = urlopen(Request(url, headers={'User-Agent': 'Mozilla/5.0'}), timeout=timeout)
            if opened_url.url != url:
                click.secho(f"WARNING: Opened URL ({opened_url.url}) is different from the requested one ({url})", fg='yellow')
            if verbose:
                click.secho(f"l.{line_number} [{opened_url.getcode()}] {url}", fg='green')
            return 0
        except HTTPError as e:
            click.secho(f"l.{line_number} [{e.code}] HTTPError for {url}", fg='red')
        except URLError as e:
            click.secho(f"l.{line_number} URLError for {url} - Reason: {e.reason}", fg='red')
        except TimeoutError as e:
            click.secho(f"l.{line_number} TimeoutError for {url}", fg='red')
    # Error management
    return 1

def _url_from_path(path: Path, verbose: bool, timeout: int) -> int:
    nb_errors = 0
    if path.is_file():
        with path.open() as f:
            try:
                click.secho(f"-- {path}")
                lines = f.readlines()
                for i, line in enumerate(lines):
                    # Specific markdown and html cases where the last parenthesis, bracket or tag is not removed
                    # Replace by a space to be sure
                    line = re.sub(r'[()<>\[\]\'\`\"]', ' ', line)
                    """Find URLs from files or directory and tests them."""
                    urls = re.findall('(?:http|ftp)s?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*(),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', line)
                    for url in urls:
                        nb_errors += _url_open(url, verbose, timeout, i+1)
            except UnicodeDecodeError as e:
                click.secho(f"WARNING: Cannot decode file using encoding \"utf-8\": {path}", fg='yellow')
    elif path.is_dir():
        for path_elt in path.iterdir():
            # recursive call
            nb_errors += _url_from_path(path_elt, verbose, timeout)

    return nb_errors

@click.command()
@click.option('--verbose', '-v', is_flag=True, help='Also display successful URLs.')
@click.option('--timeout', '-t', type=int, default=2, help='Timeout for urllib.request.urlopen. Useful when facing URLError because of timed out')
@click.argument('arguments', nargs=-1)
def _pagefound(verbose: bool, timeout: int, arguments: str) -> int:
    # Specific case when no argument is specified
    if not arguments:
        arguments=('.')
    
    nb_errors = 0
    for arg in arguments:
        split_by_star = re.split(r'(\*)', arg, maxsplit=1)
        if len(split_by_star) == 1:
            path = Path(arg)
            nb_errors += _url_from_path(path, verbose, timeout)
        # Specific for '*.html', '/home/*.md', '**/*.tex' and '/home/**/*.md'
        else:
            assert(len(split_by_star) == 3)
            path = Path(split_by_star[0])
            for file in path.glob(split_by_star[1] + split_by_star[2]):
                nb_errors += _url_from_path(file, verbose, timeout)
    
    sys.exit(nb_errors)
